<div class="row">
  <div class="col-sm-1">
    

  </div>
  

  <div class="col-sm-8">

    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-primary" >
                    <div class="panel-heading" >
                        <div class="panel-title">Sign In</div>
                        <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>
                    </div>     

                    <div style="padding-top:30px" class="panel-body" >

                        <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                            
                        <form id="loginform" class="form-horizontal" role="form" method="post" action="<?php echo base_url('login/loginAction'); ?>">
                                    
                                    <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input id="login-username" type="text" class="form-control" name="email" value="" placeholder="email">                                        
                                    </div>
                                
                                     <div style="margin-bottom: 25px" class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                        <input id="login-password" type="password" class="form-control" name="password" placeholder="password">
                                    </div>
                                    

                                
                                     <div class="input-group">
                                      <div class="checkbox">
                                        <label>
                                          <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                                        </label>
                                      </div>
                                    </div>


                                <div style="margin-top:10px" class="form-group" >
                                    <!-- Button -->

                                    <div class="col-md-offset-3 col-md-9 controls">
                                      <input type="submit" value="Login" class="btn btn-danger">

                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-12 control">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:100%" >
                                            Don't have an account! 
                                        <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
                                            Sign Up Here
                                        </a>
                                        </div>
                                    </div>
                                </div>    
                            </form>     



                        </div>                     
                    </div>  
        </div>
        <div id="signupbox" style="display:none; margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="panel-title">Sign Up</div>
                            
                        </div>  
                        <div class="panel-body" >
                            <form id="signupform" class="form-horizontal" role="form" method="post" action="<?php echo base_url("login/registerAction"); ?>">
                                
                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>
                                    
                                
                                  
                                <div class="form-group" style="margin-bottom: 5px">
                                  <label style="margin-left: 16px"><i class="fa fa-envelope-o"></i> Email Address:</label>
                                   
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="email" placeholder="Email Address">
                                    </div>
                                </div>
                                    
                                <div class="form-group" style="margin-bottom: 5px">
                                  <label style="margin-left: 16px"><i class="fa fa-user"></i> Firstname:</label>
                                   
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="firstname" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 5px">
                                  <label style="margin-left: 16px"><i class="fa fa-user"></i> Surname:</label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" name="lastname" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 5px">
                                  <label style="margin-left: 16px"><i class="fa fa-key"></i> Password:</label>
                                    <div class="col-md-12">
                                        <input type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                </div>
                                    
                                <div class="form-group" style="margin-bottom: 25px">
                                    <label style="margin-left: 16px"><i class="fa fa-key"></i> Confirm Password:</label>
                                    <div class="col-md-12">
                                        <input type="password" class="form-control" name="passwordconf" placeholder="Password Confirmation">
                                    </div>
                                </div>

                                <div class="form-group" >
                                    <!-- Button --> 
                                    <div class="col-md-offset-3 col-md-9">
                                        <input type="submit" value="Sign Up" class="btn btn-danger">
                                        <div style="border-top: 1px solid#888; padding-top:15px; font-size:100%" >
                                            <div style="float:right; font-size: 85%; position: relative; top:-10px">
                                              <a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a>
                                            </div>
                                       </div>
                                </div>
                           
                            </form>
                         </div>
                    </div>

               
               
                
         </div>
    

  </div>
  <div class="col-sm-3">
      <h5>Terms & Conditions</h5>

  </div>
</div>