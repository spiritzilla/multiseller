<h1>This is a full profile page for a product number: <?php echo $productId; ?> !</h1>
<div class="container product-main">
		<div class="row">

			<div class="col-sm-6">
				<div class="content">
				
					<!-- Product information for small screens -->
					<div class="product-details-small">
						<!-- Product name and manufacturer -->
						<h1>BeachFront Frog</h1>
						<small>by <a href="">Frogtastic</a></small>

						<!-- Product rating and review info -->
						<div class="ratings clearfix">
							<div class="rateit" data-rateit-value="4.6" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
							<div class="extra">
								<a href="#reviews">Read all 10 reviews</a> | <a href="review-product.html">Write a review</a>
							</div>
						</div>
					
						<!-- Pricing and offer info -->
						<div class="pricing clearfix">
							<p class="price"><span class="cur">$</span><span class="total">20.00</span> <span class="delivery">&amp; free delivery</span></p>
							<p class="special">2 swimsuits for $35</p>
						</div>
					</div>
				
					<div class="main-imgs clearfix">
						<a href="img/product-1.jpg" title="BeachFront Frog swimsuit: view 1"><img id="img1" src="<?php echo base_url('assets/img/test.jpg'); ?> " alt="BeachFront Frog swimsuit" class="main-img img-responsive" /></a>
						<a href="img/product-2.jpg" title="BeachFront Frog swimsuit: view 2"><img id="img2" src="<?php echo base_url('assets/img/main-hero.png'); ?> " alt="BeachFront Frog swimsuit" class="main-img img-responsive background" /></a>
					</div>
					<ul class="alternate-images clearfix">
						<li><a href="#" data-img="img1"><img src="img/product-1-mini.jpg" alt="" /></a></li>
						<li><a href="#" data-img="img2"><img src="img/product-2-mini.jpg" alt="" /></a></li>
					</ul>
				</div>
			</div> <!-- // end span6 -->

			<div class="col-sm-6">
				<div class="content">
					
					<!-- Product information for large screens -->
					<div class="product-details-large">
						<!-- Product name and manufacturer -->
						<h1>BeachFront Frog</h1>
						<small>by <a href="">Frogtastic</a></small>

						<!-- Product rating and review info -->
						<div class="ratings clearfix">
							<div class="rateit" data-rateit-value="4.6" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
							<div class="extra">
								<a href="#reviews">Read all 10 reviews</a> | <a href="review-product.html">Write a review</a>
							</div>
						</div>
					
						<!-- Pricing and offer info -->
						<div class="pricing clearfix">
							<p class="price"><span class="cur">$</span><span class="total">20.00</span> <span class="delivery">&amp; free delivery</span></p>
							<p class="special">2 swimsuits for $35</p>
						</div>
					</div>
					
					<!-- Product options -->
					<form class="form-inline clearfix cart" action="#">
						<div>
							<label>Quantity</label>	
							<input type="text" class="input-sm form-control">
						</div>
						<div>
							<label>Size</label>
							<select class="input-sm form-control">
								<option>S</option>
								<option>M</option>
								<option>L</option>										
							</select>
						</div>							
						<button class="btn btn-large btn-atc">Add to cart</button>
					</form>
					
					<!-- Product description etc -->
					<ul class="nav nav-tabs" id="product-tabs">
						<li class="active"><a href="#description">Description</a></li>
						<li><a href="#care">Care</a></li>
						<li><a href="#sizing">Sizing</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="description">
							<p>Bacon ipsum dolor sit amet t-bone corned beef brisket, chicken rump jerky meatloaf venison andouille ground round pig beef shankle pork. Pork loin tenderloin flank, swine rump chicken sausage leberkas pig biltong pancetta tongue tail bresaola. Biltong pastrami jerky, capicola brisket sausage ribeye drumstick shoulder leberkas beef sirloin. Strip steak fatback drumstick tri-tip corned beef bresaola.</p>
							<p>Cow fatback jerky shank tail jowl turkey brisket frankfurter doner pig kielbasa. Jerky cow turkey short ribs beef. Hamburger rump meatloaf brisket. Rump andouille tail kielbasa. Pig venison swine, shankle biltong chicken spare ribs doner cow. Tri-tip pastrami pork loin, pig ham hock salami sausage flank ball tip frankfurter doner meatloaf filet mignon shoulder. Shank pork tongue pig.</p>
						</div>
						<div class="tab-pane" id="care">
							<p>Rinse your swimsuit in cold water after use and allow it to dry naturally, away from direct heat and sunlight.</p>
							<p>After rinsing out the suit, you must wash your suit. Plain water does not remove all the salt or chlorine. Fill a sink with water and add just a tablespoon or less of liquid detergent. Don't use powders because they may not dissolve completely or rinse away well. And, never use bleach. Turn your swimsuit inside out. Swish for several minutes and then rinse well. Gently squeeze - don't wring - the water out of the fabric. Spread your suit flat to dry in a spot out of direct sunlight.</p>
						</div>
						<div class="tab-pane" id="sizing">
							<p>Standard swim suit sizing comes in S/M/L and come in Body measurements are given in inches. If your body measurement is on the borderline between two sizes, order the lower size for a tighter fit or the higher size for a looser fit.</p>
							<p>If your body measurements for hip and waist result in two different suggested sizes, order the size from your hip measurement. </p>
							<table class="table table-striped table-bordered">
								<tr>
									<th></th><th>Size</th><th>Bust (in)</th><th>Ribcage (in)</th><th>Waist (in)</th><th>Hip (in)</th><th>Torso (in)</th>
								</tr>
								<tr>
									<td rowspan="2">S</td><td>4</td><td>33 1/2</td><td>27</td><td>25 1/2</td><td>35 1/2</td><td>58 1/2</td>
								</tr>
								<tr>
									<td>6</td><td>34 1/2</td><td>28</td><td>26 1/2</td><td>36 1/2</td><td>60</td>
								</tr>
								<tr>
									<td rowspan="2">M</td><td>8</td><td>35 1/2</td><td>29</td><td>27 1/2</td><td>37 1/2</td><td>61 1/2</td>
								</tr>
								<tr>
									<td>10</td><td>36 1/2</td><td>30</td><td>28 1/2</td><td>38 1/2</td><td>63</td>
								</tr>
								<tr>
									<td rowspan="2">L</td><td>12</td><td>38</td><td>31 1/2</td><td>30</td><td>40</td><td>64 1/2</td>
								</tr>
								<tr>
									<td>14</td><td>39 1/2</td><td>33</td><td>31 1/2</td><td>41 1/2</td><td>66</td>
								</tr>
							</table>
						</div>
					</div>
					
					<!-- Share -->
					<ul class="share">
						<li>
							<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
						</li>
						<li>
							<div class="g-plusone" data-size="medium" data-annotation="none"></div>
						</li>
						<li>
							<a href="//pinterest.com/pin/create/button/?url=PAGEURL&amp;media=IMAGE&amp;description=DESCRIPTION" data-pin-do="buttonPin" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" alt=""/></a>
						</li>
						<li>
							<div class="fb-like" data-href="PAGEURL" data-send="false" data-width="120" data-show-faces="false" data-layout="button_count"></div>
						</li>
					</ul>
					
					
				</div>
			</div> <!-- // end span6 -->
			
		</div> <!-- //end row -->

		<div class="row" id="reviews">
			<div class="col-xs-12">
				<div class="review-overview">
					<div class="content clearfix">
						<h3>Reviews</h3>
						
						<!-- Review ratings breakdown -->
						<ul class="review-totals">
							<li>
								<p>5 star</p>
								<div class="meter">
									<span style="width: 50%"></span>
								</div>
								<p>5</p>
							</li>
							<li>
								<p>4 star</p>
								<div class="meter">
									<span style="width: 30%"></span>
								</div>
								<p>3</p>
							</li>
							<li>
								<p>3 star</p>
								<div class="meter">
									<span style="width: 20%"></span>
								</div>
								<p>2</p>
							</li>
							<li>
								<p>2 star</p>
								<div class="meter">
									<span style="width: 0%"></span>
								</div>
								<p>0</p>
							</li>
							<li>
								<p>1 star</p>
								<div class="meter">
									<span style="width: 0%"></span>
								</div>
								<p>0</p>
							</li>
						</ul>
							
						<!-- Review average -->
						<div class="review-main">
							<div class="rateit bigstars" data-rateit-value="4.6" data-rateit-ispreset="true" data-rateit-readonly="true" data-rateit-starwidth="32" data-rateit-starheight="32"></div>
							<p>4.6 out of 5 stars from 10 reviews</p>
							<p><a href="review-product.html" class="btn"><i class="icon-pencil icon-white"></i> write a review</a></p>
						</div>
					</div>
				</div>

				<div class="product-review">
	<div class="row">
		<div class="col-sm-8">
			<div class="content">
				<header>
					<div class="rateit" data-rateit-value="4.5" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
					<p><strong>Perfect, just what I wanted!</strong></p>
				</header>
				
				<p>Bacon ipsum dolor sit amet t-bone corned beef brisket, chicken rump jerky meatloaf venison andouille ground round pig beef shankle pork. Pork loin tenderloin flank, swine rump chicken sausage leberkas pig biltong pancetta tongue tail bresaola. Biltong pastrami jerky, capicola brisket sausage ribeye drumstick shoulder leberkas beef sirloin. Strip steak fatback drumstick tri-tip corned beef bresaola.</p>
				
				<footer>
					<a href="" class="btn btn-xs"><span class="glyphicon glyphicon-thumbs-up"></span> helpful</a>
					<a href="" class="btn btn-xs"><span class="glyphicon glyphicon-thumbs-down"></span> unhelpful</a>
					<a href="" class="btn btn-xs"><span class="glyphicon glyphicon-pencil"></span> comment</a>
					<a href="" class="report"><span class="glyphicon glyphicon-ban-circle"></span></a>
				</footer>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="content review-author">
				<div class="top-contrib">Top contributor</div>
				<p><a href="user-profile.html">SFShopper</a></p>
				<small>June 30th, 2013</small>	
				<p>An avid shopper, love buying my stuff from Shopfrog. <a href="user-profile.html">Read all my reviews &rarr;</a></p>
			</div>
		</div>
	</div>
</div>
				
				<div class="product-review">
	<div class="row">
		<div class="col-sm-8">
			<div class="content">
				<header>
					<div class="rateit" data-rateit-value="4.5" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
					<p><strong>Perfect, just what I wanted!</strong></p>
				</header>
				
				<p>Bacon ipsum dolor sit amet t-bone corned beef brisket, chicken rump jerky meatloaf venison andouille ground round pig beef shankle pork. Pork loin tenderloin flank, swine rump chicken sausage leberkas pig biltong pancetta tongue tail bresaola. Biltong pastrami jerky, capicola brisket sausage ribeye drumstick shoulder leberkas beef sirloin. Strip steak fatback drumstick tri-tip corned beef bresaola.</p>
				
				<footer>
					<a href="" class="btn btn-xs"><span class="glyphicon glyphicon-thumbs-up"></span> helpful</a>
					<a href="" class="btn btn-xs"><span class="glyphicon glyphicon-thumbs-down"></span> unhelpful</a>
					<a href="" class="btn btn-xs"><span class="glyphicon glyphicon-pencil"></span> comment</a>
					<a href="" class="report"><span class="glyphicon glyphicon-ban-circle"></span></a>
				</footer>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="content review-author">
				<div class="top-contrib">Top contributor</div>
				<p><a href="user-profile.html">SFShopper</a></p>
				<small>June 30th, 2013</small>	
				<p>An avid shopper, love buying my stuff from Shopfrog. <a href="user-profile.html">Read all my reviews &rarr;</a></p>
			</div>
		</div>
	</div>
</div>
				
				<div class="product-review">
	<div class="row">
		<div class="col-sm-8">
			<div class="content">
				<header>
					<div class="rateit" data-rateit-value="4.5" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
					<p><strong>Perfect, just what I wanted!</strong></p>
				</header>
				
				<p>Bacon ipsum dolor sit amet t-bone corned beef brisket, chicken rump jerky meatloaf venison andouille ground round pig beef shankle pork. Pork loin tenderloin flank, swine rump chicken sausage leberkas pig biltong pancetta tongue tail bresaola. Biltong pastrami jerky, capicola brisket sausage ribeye drumstick shoulder leberkas beef sirloin. Strip steak fatback drumstick tri-tip corned beef bresaola.</p>
				
				<footer>
					<a href="" class="btn btn-xs"><span class="glyphicon glyphicon-thumbs-up"></span> helpful</a>
					<a href="" class="btn btn-xs"><span class="glyphicon glyphicon-thumbs-down"></span> unhelpful</a>
					<a href="" class="btn btn-xs"><span class="glyphicon glyphicon-pencil"></span> comment</a>
					<a href="" class="report"><span class="glyphicon glyphicon-ban-circle"></span></a>
				</footer>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="content review-author">
				<div class="top-contrib">Top contributor</div>
				<p><a href="user-profile.html">SFShopper</a></p>
				<small>June 30th, 2013</small>	
				<p>An avid shopper, love buying my stuff from Shopfrog. <a href="user-profile.html">Read all my reviews &rarr;</a></p>
			</div>
		</div>
	</div>

	<div class="row">
			<div class="col-xs-12">
				<div class="content clearfix">
					<h3>Related products</h3>
					
					<div class="product medium">
	<div class="media">
		<a class='current' href="product.html" title="product title">
			<img src="img/product-1.jpg" alt="product title" data-img="product-1" class="img-responsive" />
		</a>
		<span class="plabel">just in</span>				
	</div>
	<div class="details">
		<p class="name"><a class='current' href="product.html">BeachSide Frog</a></p>
		<p class="price"><span class="cur">$</span><span class="total">20.00</span></p>
		<a href="" class="details-expand" data-target="details-0001">+</a>
	</div>
	<div class="details-extra" id="details-0001">
		<form class="form-inline" action="#">
			<div>
				<label>Quantity</label>	
				<input type="text" class="input-sm form-control quantity" value="1">
			</div>
			<div>
				<label>Size</label>
				<select class="input-sm form-control size">
					<option>S</option>
					<option>M</option>
					<option>L</option>										
				</select>
			</div>
		</form>
		<button class="btn btn-bottom btn-atc qadd">Add to cart</button>			
	</div>
</div>
<div class="product medium">
	<div class="media">
		<a class='current' href="product.html" title="product title">
			<img src="img/product-2.jpg" alt="product title" data-img="product-2" class="img-responsive" />
		</a>
	</div>
	<div class="details">
		<p class="name"><a class='current' href="product.html">BeachSide Frog</a></p>
		<p class="price"><span class="cur">$</span><span class="total">20.00</span></p>
		<a href="" class="details-expand" data-target="details-0002">+</a>
	</div>
	<div class="details-extra" id="details-0002">
		<form class="form-inline" action="#">
			<div>
				<label>Quantity</label>	
				<input type="text" class="input-sm form-control quantity" value="1">
			</div>
			<div>
				<label>Size</label>
				<select class="input-sm form-control size">
					<option>S</option>
					<option>M</option>
					<option>L</option>										
				</select>
			</div>
		</form>
		<button class="btn btn-bottom btn-atc qadd">Add to cart</button>			
	</div>
</div>
<div class="product medium">
	<div class="media">
		<a class='current' href="product.html" title="product title">
			<img src="img/product-1.jpg" alt="product title" data-img="product-1" class="img-responsive" />
		</a>
	</div>
	<div class="details">
		<p class="name"><a class='current' href="product.html">BeachSide Frog</a></p>
		<p class="price"><span class="cur">$</span><span class="total">20.00</span></p>
		<a href="" class="details-expand" data-target="details-0003">+</a>
	</div>
	<div class="details-extra" id="details-0003">
		<form class="form-inline" action="#">
			<div>
				<label>Quantity</label>	
				<input type="text" class="input-sm form-control quantity" value="1">
			</div>
			<div>
				<label>Size</label>
				<select class="input-sm form-control size">
					<option>S</option>
					<option>M</option>
					<option>L</option>										
				</select>
			</div>
		</form>
		<button class="btn btn-bottom btn-atc qadd">Add to cart</button>			
	</div>
</div>
<div class="product medium">
	<div class="media">
		<a class='current' href="product.html" title="product title">
			<img src="img/product-2.jpg" alt="product title" data-img="product-2" class="img-responsive" />
		</a>
	</div>
	<div class="details">
		<p class="name"><a class='current' href="product.html">BeachSide Frog</a></p>
		<p class="price"><span class="cur">$</span><span class="total">20.00</span></p>
		<a href="" class="details-expand" data-target="details-0004">+</a>
	</div>
	<div class="details-extra" id="details-0004">
		<form class="form-inline" action="#">
			<div>
				<label>Quantity</label>	
				<input type="text" class="input-sm form-control quantity" value="1" >
			</div>
			<div>
				<label>Size</label>
				<select class="input-sm form-control size">
					<option>S</option>
					<option>M</option>
					<option>L</option>										
				</select>
			</div>
		</form>
		<button class="btn btn-bottom btn-atc qadd">Add to cart</button>			
	</div>
</div>

					
				</div>
			</div> <!-- //end span12 -->
		</div> <!-

