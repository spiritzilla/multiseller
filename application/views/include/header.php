
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php  $this->load_page->session_head($secure); ?>
    <meta charset="utf-8">
    <title><?php echo (isset($page_title))? $page_title." - Multiseller Site! " : "  Africa's largest Multiseller Online Market "; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" media="screen">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootswatch.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/fontawesome/css/font-awesome.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/iyalaje.custom.css'); ?>">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../bower_components/html5shiv/dist/html5shiv.js"></script>
      <script src="../bower_components/respond/dest/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
 <p style="color:red;">
    <?php echo $secure == true ? 'Secured Page - Dev Purpose Only - Will be deleted on Production' : 'Unsecured Page - Dev Purpose Only - Will be deleted on Production' ;  ?>
</p>
    <div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo base_url('marketer/dashboard'); ?>" class="navbar-brand"> <i class="fa fa-home"></i> My Store</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes"><i class="fa fa-shopping-cart"></i> Shopping Cart <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <li><a href="../default/">Default</a></li>
                <li class="divider"></li>
                <li><a href="../cerulean/">Cerulean</a></li>
                <li><a href="../cosmo/">Cosmo</a></li>
              </ul>
            </li>
            <li>
              <a href="../help/"> <i class="fa fa-h-square"></i> Help</a>
            </li>

            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="download"> <i class="fa fa-users"></i> Community <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="download">
                <li><a href="./bootstrap.min.css"><i class="fa fa-question"></i> FAQs</a></li>
                <li><a href="./bootstrap.css"><i class="fa fa-users"></i> Forum</a></li>

              </ul>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes"><i class="fa fa-th"></i> My Account <span class="caret"></span></a>
              <ul class="dropdown-menu" aria-labelledby="themes">
                <li><a href="<?php echo base_url('marketer/profile'); ?>"><i class="fa fa-user"> </i> View Profile</a></li>
                <li><a href="<?php echo base_url('marketer/inbox'); ?>"><i class="fa fa-inbox"></i> Inbox</a></li>
                <li><a href="<?php echo base_url('login/register'); ?>"><i class="fa fa-hand-o-right"></i> Register</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo base_url('login/logoutAction'); ?>"><i class="fa fa-minus-circle"></i> logout</a></li>
              </ul>
            </li>
            <li>
              <a href="<?php echo base_url('marketer/login'); ?>"><i class="fa fa-key"></i> Login</a>
            </li>
          </ul>

        </div>
      </div>
    </div>


    <div class="container">