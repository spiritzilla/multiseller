    <footer>
        <div class="row">
          <div class="col-lg-12">

            <ul class="list-unstyled">
              <li class="pull-right"><a href="#top">Back to top</a></li>
              <li><a href="https://twitter.com/spiritzilla"><i class="fa fa-twitter"></i> Twitter</a></li>
              <li><a href="../help/#api"><i class="fa fa-spin fa-cog"></i> API</a></li>
              <li><a href="../help/#support"><i class="fa fa-life-ring"></i> Support</a></li>
            </ul>
            <p>Made by <a href="http://thomaspark.me" rel="nofollow"><i class="fa fa-btc"></i>ourne Excellensia</a>. Contact him at <a href="mailto:motolola@icloud.com"> <i class="fa fa-envelope"></i> motolola@icloud.com</a>.</p>
          
          </div>
        </div>

      </footer>


    </div>

  <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootswatch.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/iyalaje.custom.js'); ?>"></script>
  </body>
</html>