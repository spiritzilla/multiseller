<h1>List of Marketers - <span style ="color:red;">Visible to Admin Only!</span></h1>
<div class="row">

  <div class="col-sm-6">List of Marketers

  	<?php if($marketers): ?>
  	<table class="table table-striped table-condensed table-hover table-bordered">
  		<tr><th>Marketer ID</th><th>Full Name</th><th>Email</th></tr>
  		<?php foreach ($marketers as $marketer): ?>
  	
  		<tr>
  			<td><?php echo $marketer->id; ?></td>
  			<td><?php echo $marketer->firstname.' '.$marketer->lastname; ?></td>
  			<td><?php echo $marketer->email; ?></td>
  		</tr>

		<?php endforeach; ?>
  	</table>

 	 <?php endif; ?>

  </div>
  <div class="col-sm-4">.col-md-6

  </div>


</div>