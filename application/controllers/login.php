<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {

/*
	@Author: Akinjide Agboola
*/
	function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }
	
	public function index()
	{
		$this->load_page->display('login/login');
	   
	}

	/*
		This is the main registration page for the site.
	*/

	public function register()
	{
		$this->load_page->display('marketer/register');

	}
	public function registerAction()
	{

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[marketer.email]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|matches[passwordconf]');
		$this->form_validation->set_rules('passwordconf', 'Password Confirmation', 'required|min_length[6]');
		$this->form_validation->set_rules('firstname', 'Firstname', 'required|alpha_dash');
		$this->form_validation->set_rules('lastname', 'Lastname', 'required|alpha_dash');

		if ($this->form_validation->run() == TRUE)
		{

			$this->user_model->registerUser($this->input->post());

			//Send User a registration Email to confirm email


			//redirect to a page where they can confirm email
		} else {

			self::index(); //fill forms with errors while Registration pages is displayed;
			
		}
	}
	/*
		A page to allow users to reset their passwords
	*/

	public function resetpassword()
	{
		$this->load_page->display('marketer/resetpassword');
	}
	/*
		A page to allow users to login
	*/

	public function login()
	{
		$data['data'] = 'hello';
		$this->load_page->display('marketer/login',$data,false);
	}

	public function loginAction()
	{
		//var_dump($this->input->post());

		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			//echo "Pass!";exit();
			$this->user_model->login($this->input->post());

			//Send User a registration Email to confirm email
		}
		else
		{
			///$this->load->view('myform'); send errors
			self::index(); 
			
		}
	}
	public function logoutAction()
	{
		$this->session->sess_destroy();

		redirect(base_url());
	}
}