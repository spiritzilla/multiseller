<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
	@Author: Akinjide Agboola
*/

class Product extends CI_Controller {

	function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    	echo "Product/Site Home Page!<br/>";

    	$this->load_page->display('product/product_list');
    }

    public function productpage($id)
    {
        if(is_numeric($id)){

             $data['productId'] = $id;
             $this->load_page->display('product/product_page',$data); 
          
        }else{

            redirect(base_url());
        }
        
    }
}