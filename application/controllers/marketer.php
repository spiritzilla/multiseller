<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
	@Author: Akinjide Agboola
*/

class Marketer extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('marketer_model');
    }

    /*
		only accessible by admin only, it shows the list of marketers on the
	*/

	public function index()
	{
        $data['marketers'] = $this->marketer_model->getMarketers();
		$this->load_page->display('marketer/list',$data,true);
		
	}
	/*
		The profile ...
	*/

	public function profile($id = null)
	{
		$data['marketer'] = $this->marketer_model->getMarketer($id);
		$this->load_page->display('marketer/profile', $data);
	}
	/*
		The  lists all Marketer's Products
		to the system
	*/

	public function myproducts($id)
	{
		$this->load_page->display('marketer/myproducts');

	}
	/*
		The Edit Profile page is where the marketer self edit their profile
	*/

	public function editprofile()
	{
		$this->load_page->display('marketer/editdetails');

	}




}