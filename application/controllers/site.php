<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
	@Author: Akinjide Agboola
*/

class Site extends CI_Controller {

	function __construct()
    {
        parent::__construct();
    }

    public function index(){

    	echo "Index";
    }

    public function errorpage()
    {
    	$this->load_page->display('include/error_404');
    }

}