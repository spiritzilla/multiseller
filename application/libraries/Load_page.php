<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Load_page
{
    
    function display($page, $view_data = false, $secure = false)
    {
        $CI = &get_instance();

        settype($secure, 'bool');

            $security['secure'] = $secure;

        //build page , Secure = 'true' if password is needed for page, false by default
        $CI->load->view('include/header',$security);
        //build page and data
        $CI->load->view($page, $view_data);
        //build footer
        $CI->load->view('include/footer');
    }
	

   
	
    function session_head($secure = false)
    {  

        $CI = &get_instance();

        if($secure == true){

                
            if (!$CI->session->userdata('id')) {   

                redirect(base_url('login'));

            }

            } else {

                return false;
        }
		
       
    }

}

/* End of file Load_page.php */
