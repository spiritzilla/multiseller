<?php if (!defined('BASEPATH'))exit('No direct script access allowed');

/*
	@Author: Akinjide Agboola
*/

class user_model extends CI_Model
{

    function __construct(){
        parent::__construct();
    }

    public function login(array $data)
    {
    	     // grab user input
        $email = $this->security->xss_clean($data['email']);
        $password = $this->security->xss_clean($data['password']);

        // Prep the query
        $this->db->where(array('email'=> $email,'password'=> md5($password)));
       // $this->db->where('password', md5($password));
        // Run the query
        $query = $this->db->get('marketer');
        // Let's check if there are any results
        if ($query->num_rows() == 1) {
            // If there is a user, then create session data
           
            $row = $query->row();
            //more data about the user can be added to session from the hr table
            $data = array(
                'id' => $row->id,
                'firstname' => $row->firstname,
                'lastname' => $row->lastname,
                'email' => $row->email,
                'status' =>$row->status, 
                'validated' => true);
                
            if($row->status!="Active"){ //kill session if employee not active

                $this->session->sess_destroy();

                return "Not Active";

               }

            $this->session->set_userdata($data);
                

    		var_dump($this->session->all_userdata());exit();
        }
        // If the previous process did not validate
        // then return false.
        return false;

    }
/*
	The following function registers New user into the Database, Validation Must have been done from the Controller.
*/

    function registerUser(array $data)
    {

		return $this->db->insert('marketer',array(
		    		'firstname' => $data['firstname'],
		    		'lastname' => $data['lastname'],
		    		'email' => $data['email'],
		    		'password' => md5($data['password'])));

    }

}