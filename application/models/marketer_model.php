<?php if (!defined('BASEPATH'))exit('No direct script access allowed');

/*
	@Author: Akinjide Agboola
*/

class marketer_model extends CI_Model
{

    function __construct(){
        parent::__construct();
    }

    function getMarketers()
    {
    	$query = $this->db->get('marketer');

    	if($query->num_rows() > 0){

    		return $query->result();
    	}
    	
    }

    function getMarketer($id)
    {
    	$query = $this->db->get_where('marketer',array('id'=>$id));
    	if($query->num_rows() ==1){

    		return $query->result()[0];
    	}
    }
}